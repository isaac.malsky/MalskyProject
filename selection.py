# import python packages
import numpy as np
from random import randint as rand_int
import random

# import ASE functions
from ase import Atoms, Atom, build
from ase.io import read,write


def get_parents_metropolis(file_list, data_list, global_data_list,
                          structure_filename, delta_list, num_parents):
    """Chooses parent from directory based on the metropolis criteria
    Args:
        file_list (list): list of all models
        data_list (list): list of all data for models
        global_data_list (list): list of best value for each data type
        structure_filename (str): filename of structure
        delta_list (list): list of delta values for each objective
        num_parents (int): number of parents to choose

    Returns:
        parents (list): list with ASE atoms objects to be used as parents
    """
    # make parents list
    parents = []

    # choose parents
    for i in range(0, num_parents):

        # while look to test parents for usage in mating
        accept = False

        while accept == False:
            # randomly choose model
            ri = rand_int(0, len(data_list) - 1)

            # find data file
            data = data_list[ri]

            # check if model is accepted as parent for any data type
            for j in range(0, len(data_list[0])):
                acceptance = monteCarlo(data[j],
                                                 global_data_list[j],
                                                 T=delta_list[j])

                # if parent is accepted for one, it is accepted overall
                if acceptance:
                    accept = True

        # load parent
        parent_name = file_list[ri] + '/' + structure_filename
        parents.append(read(parent_name))
    return parents

def monteCarlo(obj,globalObj,T):
    """ Determines whether or not new value should be accepted based

    Args:
        obj (Float): current objective value being tested
        globalObj (Float): Objective value used as comparison
        T (Float): Temperature Criteria used for acceptance

    Returns:
        acceptance (Bool): True/False for acceptance
    """

    # calculate probability
    deltaObj = obj - globalObj
    prob = min(1,np.exp(-deltaObj/T))

    # determine acceptance
    if random.random() < prob:
        acceptance = True
    else:
        acceptance = False

    return acceptance