# Import general python functions
import numpy as np
import math
import os, sys

# import specific functions
import shutil
from subprocess import call
import time
import random
from random import randint
from random import uniform as unif
from numpy.random import choice

# import other FANTASTX functions
import manage_io
import selection
import change_structure

# update system path
sys.path = [os.getcwd()+'/ase_mazay'] + sys.path

# import ASE functions
from ase import Atoms, Atom, build
from ase.io import read,write
import ase.constraints
from ase.data import atomic_numbers, chemical_symbols, atomic_masses



class optimize:
    """Generates an class object to optimize atomistic structure based
    on initial input. This class object can be used for basinhopping or
    genetic algorithm based on the input specified

    Args:
        atoms_object (ASE atoms object): initial atomistic structure of system
        params (dict): parameter dictionary used for controlling optimization

    Returns:
        None. All output is saved to files as indicated by input parameters.
    """

    def __init__(self,atoms_object, params):
        """Initializer for optimize class.

        Args:
            atoms_object (ASE atoms object): initial atomistic structure of
                                             system
            params (dict): parameter dictionary used for controlling
                           optimization

        Returns:
            None. Updates self object for optimize class.
        """

        # settings for optimization type
        self.change_function = params.get('change_function')
        self.select_function = params.get('select_function')
        self.gc_operations = params.get('gc_operations')
        self.gc_check = params.get('gc_check', 50)

        # optimization parameters
        self.delta_list = params.get('delta_list')
        self.delta_bool_list = params.get('delta_bool_list')
        self.num_parents = params.get('num_parents')

        # settings for optimization region
        self.jump_size = params.get('jump_size',0)
        self.box_size = params.get('box_size',[[0, 1], [0, 1], [0, 1]])
        self.atom_order = params.get('atom_order')

        # settings for optimization data types
        self.data_functions = params.get('data_functions')
        self.files = params.get('files')
        self.parameters = params.get('parameters')

        # settings for distance check
        self.dist_function = params.get('dist_function')
        self.dist_compare = params.get('dist_compare')

        # settings for mutation (default is no mutation)
        self.only_mutate = params.get('only_mutate', 0)
        self.mut_list = params.get('mut_list', [0, 0, 0])
        self.mutate_rate = params.get('mutate_rate', 0)

        # setting for run
        self.run_time = params.get('run_time', 3000)
        self.calc_time = params.get('calc_time', 10)

        # settings for data storage
        self.data_name = params.get('data_name','data_file.txt')
        self.directory = params.get('directory')
        self.sub_dir = params.get('sub_dir')
        self.structure_filename = params.get(
            'structure_filename','structure.cif')

    def data_check(self, atoms_obj, directory):
        """Checks the data of the current model
        Args:
            atoms_obj (ASE atoms object): Input ASE atoms object
            directory (str): directory where run will performed and saved

        Returns:
            atomsObj (ASE atoms object): Output ASE atoms object
        """

        # get base directory location and change to model directory
        cwd = os.getcwd()
        os.chdir(directory)

        # copy over all necessary files
        for file in self.files:
            if type(file) is str:
                shutil.copyfile(
                    src=cwd + '/' + file, dst=os.getcwd() + '/' + file)

            # if several files are needed, copy all over files
            elif type(file) is list:

                # iterate over list of files
                for f in file:
                    if type(f) is str:
                        shutil.copyfile(
                            src=cwd + '/' + f, dst=os.getcwd() + '/' + f)

        # evaluate all data types and append to data
        data = []
        for i in range(0, len(self.data_functions)):
            atoms_obj, data_fxn = self.data_functions[i](atoms_obj,
                                                         self.parameters[i],
                                                         self.files[i])

            # add data to list
            data.append(data_fxn)

        # save all data for the model
        np.savetxt('data', data)
        write(self.structure_filename, atoms_obj)

        # return to base directory
        os.chdir(cwd)

        return atoms_obj, data

    def no_generation(self):
        """Perform optimization using a no-generation optimizer.
        Operates on a continually updating pool of structures and chooses
        candidates based on a probability distribution. Allows multiple
        instances of no-generation optimizers to operate on the same pool of
        structures concurrently, making them easily parallelizable.

        Args:
            None. Uses self object from optimize class.

        Returns:
             None. All output is saved to files as indicated by input
             parameters.
        """

        # get initial time
        start_time = time.time()

        # find number of files created already, used for restarting runs
        num = manage_io.get_model_number(self.directory, self.sub_dir)

        # only iterate if there is sufficient time remaining
        time_bool = True
        while time_bool == True:

            # load data from all structures
            file_list, data_list = manage_io.load_no_gen_data(
                self.directory, self.data_name)

            # parse data from structures, find minimum value(s)
            global_data_list = manage_io.no_gen_global_vals(data_list)

            # update deltas and mutation rate
            manage_io.delta_update(
                data_list=data_list, delta_list=self.delta_list,
                delta_bool_list=self.delta_bool_list)

            #manage_io.update_mutate_no_gen(data_list=data_list)

            # check if new structure will be made with mutation only
            if random.random() < self.only_mutate:
                child_atoms = self.select_function(
                    file_list, data_list, global_data_list,
                    self.structure_filename, self.delta_list,
                    self.num_parents)

                child_atoms = self.mut_list[randint(0,len(self.mut_list)-1)](
                    child_atoms, self.jump_size, self.dist_function, self.dist_compare,
                    self.box_size)

            # otherwise, it will be from main operation
            else:

                # get parents and mate them together
                parents = self.select_function(
                    file_list,data_list, global_data_list,
                    self.structure_filename, self.delta_list,
                    self.num_parents)

                child_atoms = self.change_function(
                    parents,self.jump_size, self.dist_function,
                    self.dist_compare, self.box_size)

                # mutate the offspring, based on probability
                if random.random() < self.mutate_rate:
                    child_atoms = self.mut_list[
                        randint(0, len(self.mut_list) - 1)](
                        [child_atoms], self.jump_size, self.dist_function,
                        self.dist_compare, self.box_size)

            # make new directory
            directory = manage_io.make_sub_dir(
                self.directory, self.sub_dir, num)

            num = num + 1
            if not os.path.exists(directory):
                os.makedirs(directory)

            # check data
            atomsObj, data = self.data_check(child_atoms, directory)

            # save data to data file
            filename = directory
            manage_io.update_data_file(
                filename, data, self.directory, self.data_name)

            # check to see if there is enough time for another model
            current_time = (time.time() - start_time)
            time_bool = manage_io.check_time(
                self.run_time, current_time, self.calc_time)

        return