# import python packages
import os
import sys
import numpy as np
import shutil

# update path for ASE
sys.path = [os.getcwd()+'/ase_mazay'] + sys.path

# import FANTASTX packages
import manage_io

# import ASE packages
from ase.io import read, write
from ase import Atoms, Atom, build


def check_data(atoms_obj, directory, fxn, parameters, files, struct_file_name,
               data_file_name):
    """Checks the data of the current model

    Args:
        atoms_obj (ASE atoms object): Input ASE atoms object
        directory (str): directory where run will performed and saved
        fxn (list): list of functions used to evaluate structure
        parameters (list): list of parameters passed to functions
        files (list): list of files saved to sub-directories
            that are used to run the functions
        struct_file_name (str): output structure filename
        data_file_name (str): output file name for data

    Returns:
        atoms_obj (ASE atoms object): Output ASE atoms object
    """

    # get base directory location and change to model directory
    cwd = os.getcwd()
    os.chdir(directory)

    # iterate over file list
    for file in files:

        # copy single file
        if type(file) is str:
            shutil.copyfile(src=cwd + '/' + file, dst=os.getcwd() + '/' + file)

        # copy list of files
        elif type(file) is list:
            for f in file:
                if type(f) is str:
                    shutil.copyfile(src=cwd + '/' + f, dst=os.getcwd() + '/' + f)

    # evaluate all data types and append to data
    data = []
    for i in range(0, len(fxn)):
        atoms_obj, data_value = fxn[i](atoms_obj, parameters[i], files[i])
        data.append(data_value)

    # save all data for the model
    np.savetxt('data', data)
    write(struct_file_name, atoms_obj)

    # return to base directory
    os.chdir(cwd)

    # make data file to append to
    dat = [directory]


    for d in data:
        dat.append(d)

    # save output data to file
    dataPrint = ' '.join(map(str, dat))

    with open(data_file_name, "a") as myfile:
        myfile.write(dataPrint + " \n")

    return atoms_obj



def initialize_given_models(directory, model_name, params):
    """Initialize a set of given models that are stored by directory,
    for a non-generation or sequential run.

    Args:
        directory (str): directory name where models are stored
        model_name (str): name for the structure file, must be the same for
            each structure
        params (dict): parameter dictionary used for controlling optimization,
            used for optimization and in initialization

    Return:
         None. Makes evaluates data based on data functions, saves output
         to sub-directories and creates data file
    """

    dir_list = manage_io.get_immediate_subdirectories(directory)

    print dir_list

    # generate data file name
    data_filename = directory + '/data_file.txt'

    for direct in dir_list:

        # make pathname for each model
        model_dir = os.path.join(directory, direct)
        model_path_name = os.path.join(model_dir, model_name)


        # load structure
        atoms_obj = read(model_path_name)

        atoms_obj = check_data(
            atoms_obj,
            directory=model_dir,
            fxn=params['data_functions'],
            parameters=params['parameters'],
            files=params['files'],
            struct_file_name=params['structure_filename'],
            data_file_name='data_file.txt'
        )

        # load data from run
        model_data = os.path.join(model_dir, 'data')
        data = np.loadtxt(model_data)

        # generate string form for data
        dat = [model_dir]

        if data.size > 1:
            for d in data:
                dat.append(d)

        else:
            dat.append(data)

        data_print = ' '.join(map(str, dat))

        # append data to datafile
        with open(data_filename, 'a') as f:
            f.write(data_print + ' \n')

    return


