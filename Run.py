# import general python code
import os, sys
#sys.path  = [os.getcwd()+'/../../fantastx'] + sys.path
sys.path  = [os.getcwd()+'/ase_mazay'] + sys.path
from ase.io import read
import time

# import FANTASTX code that Spencer mostly wrote
import initialize
import optimize
import change_structure
import data_functions
import distance_check as dc
import selection

# get start time
start_time = time.time()

# load structure
atomsObj = read('Au.cif')

# set up parameters for run- make a dictionary to pass to basin-hopping class
params ={
    'change_function': change_structure.bi_parent_cluster_mating_cell,
    'select_function': selection.get_parents_metropolis,
    'delta_list': [0.2],
    'delta_bool_list': [False],
    'dist_compare': [2.4, 3.4],
    'jump_size': 0.3*2.88,
    'num_parents': 2,
    'mutate_rate':0.2,
    'only_mutate':0.1,
    'mut_list':[change_structure.sphere, change_structure.cube, change_structure.scale_structure],
    'files': ["Au.tersoff"],
    'parameters': [{'pair_style': 'tersoff', 'pair_coeff': ['* *  Au.tersoff Au'],
                   'boundaries': 'p p p', 'mass': ["1 196.96657"]}],
    'data_functions': [data_functions.lammpsEnergyMin],
    'dist_function': dc.single_pair_distance,
    'directory': 'Au_basinhoppingTest',
    # string for directory to store all basin-hopping data
    # can be already made, or will be made by basin-hopping class
    'sub_dir': 'Au',
    # string for subdirectory name, will be used for each basin-hopping step
    'structure_filename': 'structure.cif'
}

initialize.initialize_given_models(params['directory'], 'Au.cif', params)

# load basinhopping class, pass structure and parameters to it
mainAlg = optimize.optimize(atomsObj, params)

# run Wale implementation of basinhopping- has cube displacement and surface 
mainAlg.no_generation()

# print out how long whole process took
print("It took %s seconds to run basinhopping" % (time.time() - start_time))
