# Import general python
import numpy as np
import os, sys, math 

import time, random
from random import randint as rndInt
from random import uniform as unif
from numpy.random import choice
from scipy.optimize import brute

# Import ASE
from ase import Atoms, Atom, build
from ase.io import read,write
import ase.constraints
from ase.data import atomic_numbers, chemical_symbols, atomic_masses
from ase.calculators.lammpsrun import LAMMPS

from ase.calculators.cp2k import CP2K
from ase.constraints import FixAtoms


def lammpsEnergyMin(atomObj, parameters=None, files=None):
    """ Function to minimize the energy of each structure in population

    Args:
        atomObj (ASE Obj): ASE atoms object with structure to look at
        parameters (Dict): Keyword pairs to be passed to lammps 
        files (List): Contains files needed for lammps run (ex: potential file)

    Returns:
        atomObj (ASE Obj): ASE atoms object with energy determined
        energy (Float): Energy of atoms object
    """

    # add lammps calculator to atoms object 
    filename = [files]

    calc = LAMMPS(files=filename,parameters=parameters,keep_alive=False)
    atomObj.set_calculator(calc)

    # pass minimize parameters to lammps calculator
    parameters['minimize'] = '1.0e-2 1.0e-1 100 3000' 
    calc.params = parameters
    
    # minimize potential energy
    atomObj.get_potential_energy()
    calc.run(set_atoms=True)
    atomObj.set_positions(calc.atoms.positions)

    # return energy and updated atomic system
    energy=atomObj.get_potential_energy()
    print (energy)
    return atomObj, energy
