# Import ASE
from ase import Atoms, Atom, build
from ase.io import read,write
import ase.constraints
from ase.data import atomic_numbers, chemical_symbols, atomic_masses


def single_pair_distance(atoms_obj, dist_compare, box_size):
    """Checks the distances of all atoms to determine if any are unbonded
    using a single pair of bounds

    Args:
        atoms_obj (ASE object): ASE atoms object input
        dist_compare (list): specifies distance constraints for atomic pairs
                             with each element as a list:
                             [element1, element2, minDist, maxDist]
        box_size (list): list of [lower bound, upper bound] pairs that
                        define the region of operation

    Returns:
        bool (bool): Boolean indicating if any atoms do not have a bond
                    False: no atoms are un-bonded
                    True: one or more atoms are unbonded
    """
    # limit distance constraints to box
    atoms_obj = create_box(atoms_obj, dist_compare, box_size)

    # get list of interatomic distances from ASE atoms object
    distances = atoms_obj.get_all_distances()
    min_dist_all = []

    # add nearest neighbor distance for each atom to array
    for atm in range(len(atoms_obj)):
        d = filter(lambda a: a != 0, distances[atm])
        min_dist_all.append(min(d))

    # check to make sure that all atoms are accounted for
    if len(min_dist_all) < len(atoms_obj):
        bool = True

    # check to see that all nearest neighbors are acceptable bond lengths
    elif min(min_dist_all) >= dist_compare[0] and max(min_dist_all) <= dist_compare[1]:
        bool = False

    else:
        bool = True

    return bool


def create_box(atoms_obj, dist_compare, box_size):
    """Creates the box for distance checking.
    Returns

    Args:
        atoms_obj (ASE object): ASE atoms object input
        dist_compare (list): specifies distance constraints for atomic pairs
                             with each element as a list:
                             [element1, element2, minDist, maxDist]
        box_size (list): list of [lower bound, upper bound] pairs that
                        define the region of operation

    Returns:
        atoms_obj (ASE object): ASE atoms object input for distance check
    """

    # return full structure
    if box_size == [[0, 1], [0, 1], [0, 1]]:
        return atoms_obj

    # return structure based on box constraints and bond length
    else:
        new_box = []
        for side in box_size:

            # check for whole dimension
            if side is [0, 1]:
                new_box.append(side)

            # add part of the box, but with extra room for bond length
            else:

                # if multi-species, find largest lower boundary for distance
                if type(dist_compare[0]) is list:
                    min_dist = 0
                    for pair in dist_compare:
                        if pair[0] > min_dist:
                            min_dist = pair[0]

                    new_box.append([side[0]-min_dist, side[1]+min_dist])

                # if single species, then use only minimum distance
                else:
                    new_box.append([side[0]-dist_compare[0],
                                    side[1]+dist_compare[0]])

        # define new box limit in cartesian coordinates
        lens = atoms_obj.get_cell_lengths_and_angles()
        new_box_cart = [(new_box[0] * lens[0]), (new_box[1] * lens[1]),
                        (new_box[2] * lens[2])]

        # remove atoms that are not in the box
        del atoms_obj[[x for x, atom in enumerate(atoms_obj)
                       if not all(new_box_cart[i][0] <= atom.position[i]
                       <= new_box_cart[i][1] for i in range(0, 3))]]

        return atoms_obj