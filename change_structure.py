# Import necessary supporting code and functions
import sys, os, math, random, copy
import numpy as np
from numpy.random import choice
from numpy.random import normal as norm
from numpy.random import uniform as unif
from random import randint as rand_int
import time

# import ASE code
from ase import Atoms, Atom, build
from ase.io import read,write


def bi_parent_cluster_mating_cell(
        parents, jump_size, dist_check, dist_compare,
        box_size=[[0, 1], [0, 1], [0, 1]]):
    """Mate two parents by splitting them in half
    and combining one half from each.

    Args:
        parents (list): List of two ASE atoms objects
        jump_size (float): maximum jump size for basinhopping
        dist_check (fxn): function used to check distance
        dist_compare (array): 2 point array, specifies [min,max] distances
        box_size (list): list of [lower bound, upper bound] pairs
                         that specifies the region in which to operate

    Returns:
        childAtoms (ASE Atoms): Returns the offspring ASE atoms object
    """

    # take parents from list
    parent1 = parents[0]
    parent2 = parents[1]

    # set distance boolean
    dist_bool = True

    # get number of atoms and cell parameters
    atom_num = parent1.get_number_of_atoms()
    cell_params = parent1.get_cell_lengths_and_angles()
    cent = (cell_params[0]/2, cell_params[1]/2, cell_params[2]/2)

    # main combine parents loop, requires distance boolean
    # to accept offspring
    while dist_bool:

        # sub-loop, requires number of atoms to remain constant
        n_atom_bool = 0
        while n_atom_bool == 0:
            # copy atoms objects, so they can be operated upon
            tmp_atoms1 = parent1.copy()
            tmp_atoms2 = parent2.copy()

            # rotate both sets of atoms randomly
            dirs = ['x', 'y', 'z']
            for direct in dirs:
                tmp_atoms1.rotate(a=unif(0, 180), v=direct, center=cent,
                                  rotate_cell=False)
                tmp_atoms2.rotate(a=unif(0, 180), v=direct, center=cent,
                                  rotate_cell=False)

            # take half of each by cell size
            del tmp_atoms1[[x for x, atom in enumerate(tmp_atoms1)
                            if atom.position[2] <= (cell_params[2]/2)]]
            del tmp_atoms2[[x for x, atom in enumerate(tmp_atoms2)
                            if atom.position[2] > (cell_params[2]/2)]]

            # check for correct number of atoms
            if (len(tmp_atoms1) + len(tmp_atoms2)) == atom_num:
                for x in range(0, len(tmp_atoms2)):
                    tmp_atoms1.append(tmp_atoms2[x])

                    # check for correct chemical composition
                if (tmp_atoms1.get_chemical_formula() ==
                        parent1.get_chemical_formula()):
                    n_atom_bool = 1

        # check for reasonable bonds
        dist_bool = dist_check(tmp_atoms1, dist_compare, box_size)

    # translate structure to make center of mass in the middle of the box
    tmp_com = tmp_atoms1.get_center_of_mass(scaled=False)
    moveDist = [cell_params[0]/2, cell_params[1]/2,
                cell_params[2]/2] - tmp_com
    tmp_atoms1.translate(moveDist)
    childAtoms = tmp_atoms1
    return childAtoms


def cube(
        parents, jump_size, dist_check, dist_compare,
        box_size=[[0, 1], [0, 1], [0, 1]]):
    """Randomly displaces atoms in a uniform distribution cube around
    there starting location.

    Args:
        parents (list): List of one ASE atoms object
        jump_size (float): maximum jump size for basinhopping
        dist_compare (fxn): function used to check distance
        dist (array): 2 point array, specifies [min,max] distances
        box_size (list): list of lists that contains min-max pairs for
                         boundaries for displaced atoms

    Returns:
        atoms_obj (ASE atoms object): Output ASE atoms object
    """
    # load initial structure
    atoms_obj = parents[0]

    # initialize distance constraint boolean
    dist_bool = True

    # attempt to change atomic arrangement until distance constraints
    # are satisfied
    while dist_bool:

        # get postions and make a copy of the ASE atoms object
        pos = atoms_obj.get_positions()
        s_pos = atoms_obj.get_scaled_positions()
        tmp_atm = atoms_obj.copy()

        # randomly displace atoms
        js = jump_size
        for dispAtom in range(0, len(atoms_obj)):

            # check if atoms are within operating region defined
            bool_list = []
            for i in range(0, 3):
                if box_size[i][0] <= s_pos[dispAtom][i] <= box_size[i][1]:
                    bool_list.append(True)

                else:
                    bool_list.append(False)

            # check if it is within displacement box
            if not any(bool_val is False for bool_val in bool_list):

                # move atoms that are within box
                pos[dispAtom] = [pos[dispAtom][0] + unif(-js, js),
                                 pos[dispAtom][1] + unif(-js, js),
                                 pos[dispAtom][2] + unif(-js, js)]

        # update positions
        tmp_atm.set_positions(pos)

        # check distance constraints after atom is moved
        dist_bool = dist_check(tmp_atm, dist_compare, box_size)

    # update atomistic system
    atoms_obj = tmp_atm

    return atoms_obj


def sphere(
        parents, jump_size, dist_check, dist_compare,
        box_size=[[0, 1], [0, 1], [0, 1]]):
    """Randomly displaces atoms in a uniform distribution sphere around
    their starting location.

    Args:
        parents (list): List of one ASE atoms object
        jump_size (float): maximum jump size for basin-hopping
        dist_check (fxn): function used to check distance
        dist (array): 2 point array, specifies [min,max] distances
        box_size (list): list of lists that contains min-max pairs for
                         boundaries for displaced atoms

    Returns:
        atoms_obj (ASE atoms object): Output ASE atoms object
    """
    # load initial structure
    atoms_obj = parents[0]

    # initialize distance constraint boolean
    dist_bool = True

    # attempt to change atomic arrangement until distance constraints
    # are satisfied
    while dist_bool:
        # get postions and make a copy of the ASE atoms object
        pos = atoms_obj.get_positions()
        s_pos = atoms_obj.get_scaled_positions()
        tmp_atm = atoms_obj.copy()

        # randomly displace atoms
        for dispAtom in range(0, len(atoms_obj)):

            # check if atoms are within operating region defined
            bool_list = []
            for i in range(0, 3):
                if box_size[i][0] <= s_pos[dispAtom][i] <= box_size[i][1]:
                    bool_list.append(True)

                else:
                    bool_list.append(False)

            # check if it is within displacement box
            if not any(bool_val is False for bool_val in bool_list):
                # get positions of atoms and define displacement vector
                theta = math.radians(unif(0, 360))
                phi = math.radians(unif(0, 180))
                r = unif(0, jump_size)

                # move atom along displacement vector
                pos[dispAtom] = [pos[dispAtom][0] + (r * math.sin(phi)
                                                     * math.cos(theta)), pos[dispAtom][1]
                                 + (r * math.sin(phi) * math.sin(theta)),
                                 pos[dispAtom][2] + (r * math.cos(phi))]

        # update positions
        tmp_atm.set_positions(pos)

        # check distance constraints after atom is moved
        dist_bool = dist_check(atoms_obj, dist_compare, box_size)

    # update atomistic system
    atoms_obj = tmp_atm
    return atoms_obj


def scale_structure(
        parents, jump_size, dist_check, dist_compare,
        box_size=[[0, 1], [0, 1], [0, 1]]):
    """Stretch or squeeze the structure

    Args:
    	parents (list): List of one ASE atoms object

    Returns:
    	atoms_obj (ASE Obj): Updated ASE atoms object
    """
    # load initial structure
    atoms_obj = parents[0]

    # determine center of mass (COM)
    com = atoms_obj.get_center_of_mass(scaled=False)
    dist_bool = True

    # flex structure distance loop
    while dist_bool:

        # load positions and make copy of atoms object
        pos = atoms_obj.get_positions()
        pos_copy = atoms_obj.get_positions()
        tmp_atm = atoms_obj.copy()

        # determine if the structure is stretched or compressed
        factor = random.choice([-0.15, 0.15])

        # define unit vector
        r = 1
        theta = math.radians(unif(0, 360))
        phi = math.radians(unif(0, 180))
        unit_vect = [(r * math.sin(phi) * math.cos(theta)),
                     (r * math.sin(phi) * math.sin(theta)), (r * math.cos(phi))]

        # update atomic coordinates based along unit vector
        for atm in range(0, len(atoms_obj)):
            pos[atm] = [(pos[atm][0] - unit_vect[0] * factor * (pos[atm][0] - com[0])), (pos[atm][1] - unit_vect[1] * factor
                         * (pos[atm][1] - com[1])), (pos[atm][2]- unit_vect[2] * factor * (pos[atm][2] -com[2]))]

        # define new unit vector orthoganol
        theta = theta + math.radians(180)
        phi = 0
        unit_vect = [(r * math.sin(phi) * math.cos(theta)), (r * math.sin(phi)
                                                             * math.sin(theta)), (r * math.cos(phi))]
        factor = -1 * factor

        # update atomic coordinates based orthoganol to previous unit vector
        for atm in range(0, len(atoms_obj)):
            pos[atm] = [(pos[atm][0] - unit_vect[0] * factor
                         * (pos_copy[atm][0] - com[0])),
                        (pos[atm][1] - unit_vect[1] * factor
                         * (pos_copy[atm][1] - com[1])),
                        (pos[atm][2] - unit_vect[2]
                         * factor * (pos_copy[atm][2] - com[2]))]

        # update positions and check distance constraint
        tmp_atm.set_positions(pos)
        dist_bool = dist_check(atoms_obj, dist_compare, box_size)

    # update atomistic system
        atoms_obj = tmp_atm
    return atoms_obj