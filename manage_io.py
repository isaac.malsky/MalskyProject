import shlex
import shutil
from pathlib2 import Path
from ase.io import write
import os
import numpy as np

def make_sub_dir(directory, sub_dir, num):
    """Makes an directory with iteration number

    Args:
        directory (str): directory where the data from the whole optimization
                         is stored
        sub_dir (str): sub-directory base name where the data from the
                       individual model is stored
        num (int): model number

    Returns:
        dest (str): destination directory
    """

    dest = directory + '/' + sub_dir + str(num)

    # check if main directory exists and create it if it does not
    if not os.path.exists(directory):
        os.makedirs(directory)

    # check if sub directory exists and create it if it does not
    if not os.path.exists(dest):
        os.makedirs(dest)

    return dest

def copy_dir_contents(src, dest):
    """Copy the contents of one directory to another.

    Args:
        src (str): source directory
        dest (str): destination directory

    Returns:
        None.
    """

    # check for destination directory, make one if it does not exist
    if not os.path.exists(dest):
        os.makedirs(dest)

    # list files in source directory
    src_files = os.listdir(src)

    # copy all files in source directory to destination directory
    for file_name in src_files:
        full_file_name = os.path.join(src, file_name)
        if (os.path.isfile(full_file_name)):
            shutil.copy(full_file_name, dest)

    return

def check_time(runtime, current_time, calc_time):
    """Checks run to see if there is time remaining, used
    to prevent optimization from ending while calculation
    or file management is occuring.

    Args:
        runtime (float): allowed runtime of optimization
        current_time (float): current time in run
        calc_time (float): approximate time of calculation and
                          file management

    Returns:
        time_bool (Bool): boolean that manages if run will end or continue
                          True: sufficient time remains, optimization continues
                          False: insufficient time remains, optimization ends
    """

    # calculate time difference after next calculation
    time_diff = runtime - current_time - 2 * calc_time

    # check if there is enough time left
    if time_diff > 0:
        time_bool = True

    # if there is not enough time, return false
    else:
        time_bool = False

    return time_bool


def get_immediate_subdirectories(directory):
    """Get the immediate subdirectory from a directory.
    Based on a stackoverflow answer by RichieHindle.

    Args:
        directory (str): directory where models are stored

    Returns:
        modelList (list): list of subdirectories where models are saved
    """

    # make list of all subdirectories
    model_list = [name for name in os.listdir(directory)
                 if os.path.isdir(os.path.join(directory, name))]

    return model_list

def sort(atoms_obj, atom_order):
    """Sorts the atoms object based on atoms order, helps prevent errors with
    codes that are order dependent, such as VASP, when the files are created

    Args:
        atoms_obj (ASE atoms object): ASE atoms object, to be sorted
        atom_order (dict): dictionary containing atom:order number pairs
                          ex: {'Element1':1,'Element2':2 ... }

    Returns:
        atoms_obj (ASE atoms object): sorted ASE atoms object
    """

    # list of chemical symbols as ASE has them stored
    symbols = atoms_obj.get_chemical_symbols()

    # make list of order numbers as ASE has them stored
    atom_nums = []
    for i in range(0, len(symbols)):
        atom_nums.append(atom_order[symbols[i]])

    # sort ASE atoms object based on order number
    permutation = np.argsort(atoms_obj.get_chemical_symbols())
    atoms_obj = atoms_obj[permutation]

    return atoms_obj


def get_order(atoms_obj):
    """Determines the order of elements in an atoms object, helps prevent
    errors with codes that are order dependent, such as VASP, when the files
    are created

    Args:
        atoms_obj (ASE atoms object): ASE atoms object, to be sorted

    Returns:
        atom_order (dict): dictionary containing atom:order number pairs
                          ex: {'Element1':1,'Element2':2 ... }
    """
    symbols = atoms_obj.get_chemical_symbols()

##### Needs to be finished!!! #########

def get_model_number(directory, sub_dir):
    """Reads throught the fit directory to find the model number
    for no generation ga process.

    Args:
        directory (str): directory name where optimization output is stored
        sub_dir (str): beginning of sub directory names for individual
                       structures

    Returns:
        num (int): number of models in directory for that process
    """

    # get list of all models in directory
    model_list = get_immediate_subdirectories(directory)

    # define list of files created by subprocess
    sub_list = []

    # find all models created by this process
    for model in model_list:
        path, file = os.path.split(model)
        if file.startswith(sub_dir):
            sub_list.append(file)

    # determine number of models create by process
    if sub_list:
        num = len(sub_list)

    else:
        num = 0

    return num

def update_data_file(filename, data, direct,data_filename):
    """Update the no generations data file

    Args:
        filename (str): filename of the model being added to data file
        data (list): list of data from model
        direct (str): location to store data file
        data_filename (str): filename for stored data

    Returns:
        None. It appends to the data file.
    """

    # make data file to append to
    dat = [filename]
    for d in data:
        dat.append(d)

    # save output data to file
    data_print = ' '.join(map(str, dat))

    # edit master data file
    data_file_loc = direct + '/'+ data_filename
    with open(data_file_loc, "a") as f:
        f.write(data_print + " \n")

    return

def no_gen_global_vals(data_list):
    """Determines the global optimimum values for all data types

    Args:
        data_list (list): list of all data for models

    Returns:
        global_data_list (list): list of best value for each data type
    """

    # find global optimum values of each data type
    global_data_list = []
    for i in range(0, len(np.array(data_list)[0, :])):
        global_data_list.append(np.min(np.array(data_list)[:, i]))

    return global_data_list

def update_mutate_no_gen(data_list):
    """Updates the mutation rate of the no generations optimization.

    Args:
        data_list (list): list of data from all models

    Returns:
        None. Updates M (nutation rate in self)
    """

    # order the lists of models
    dat = []
    for d in range(0, len(data_list[0])):
        dat.append(sorted(np.array(data_list)[:, d]))

    # initialize variable for how close together data is
    m = 0

    # iterate over 6 best solutions
    for i in range(0, 3):
        m_bool = False
        # iterate over each data type
        for j in range(0, len(data_list[0])):
            if abs(dat[j][3 * i] - dat[j][3 * i + 3]) < self.crit[j]:
                m_bool = True

        if m_bool is True:
            m = m + 1

    # update mutation rate based on how close the values are together
    # high mutation rate, if models are close together
    if m >= 3:
        self.M = self.mRange[2]

    # medium mutation rate if some are close together
    elif m >= 1 and m < 3:
        self.M = self.mRange[1]

    # low mutation rate if they are dissimilar
    elif m < 1:
        self.M = self.mRange[0]

def delta_update(data_list, delta_list, delta_bool_list):
    """Updates the delta values used for mating.

    Args:
        data_list (list): list of data from all models
        delta_list (list): list of delta values
        delta_bool (bool): boolean if delta is changed

    Returns:
        delta_list (list): list of delta values
    """

    for d in range(0, len(delta_list)):

        # update deltas for variables assigned to update
        if delta_bool_list[d]:

            # take the standard deviation of lowest 6 data point from the
            # last 30 to make new delta
            delta_list[d] = np.std(sorted(np.array(data_list)
                                              [-30:, d])[:6])

    return delta_list

def load_no_gen_data(direct,data_filename):
    """Load data from the no generation optimization data file
    Args:
        direct (str): directory of the stored data file
        data_filename (str): filename for stored data

    Returns:
        file_list (list): list of all models
        data_list (list): list of all data for models
    """

    # initialize blank arrays
    filename = direct+'/'+data_filename
    file_list = []
    data_list = []

    # read data file into list for file names and data values
    with open(filename,'r') as f:
        lines = f.readlines()
        for line in lines:
            split_line = line.split(' ')

            # load file names into list
            file_list.append(split_line[0])

            # load data into list
            splt = split_line[1:-1]
            splt_data = []

            for i in range(0,len(split_line[1:-1])):
                splt_data.append(float(splt[i]))

            # add model data to list
            data_list.append(splt_data)

    return file_list, data_list
